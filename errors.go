package formhandler

import "fmt"

var (
	ErrNoMultipart          = fmt.Errorf("error: no multipart form")
	ErrTempFile             = fmt.Errorf("error while creating temp file")
	ErrForbiddenFileType    = fmt.Errorf("error: file type in forbidden list")
	ErrInvalidRequestMethod = fmt.Errorf("error: invalid request method")
)

type ExceedsMaxFileSizeError string

func (e ExceedsMaxFileSizeError) Error() string {
	return fmt.Sprintf("file %q exceeds max file size", string(e))
}

type NoAllowedFileTypeError string

func (e NoAllowedFileTypeError) Error() string {
	return fmt.Sprintf("error: file type not allowed for field %q", string(e))
}
