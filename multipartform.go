package formhandler

import (
	"bytes"
	"io"
	"mime/multipart"
	"net/http"
	"net/url"
	"os"
	"strings"
)

// MultiPartHandler is the interface that the caller must definefor multipart forms
type MultiPartHandler interface {
	Handler

	// HandleFile is called, whenever a file was uploaded
	// - formName is the name of the corresponding form field
	// - fileName is original file name
	// - tempfile is the temporary file containing the data, it will be deleted after the call
	// If not files where uploaded, HandleFile is not called.
	// Any returned error will end up in a call to HandleError
	HandleFile(formName, fileName string, tempfile string) error
}

// NewMultiPart returns a http.Handler that can handle multipart forms (with uploading of files).
// If you don't need to upload files, consider using a simple form and call New.
func NewMultiPart(fn func() MultiPartHandler, options ...Option) http.Handler {
	return &multipartForm{
		form:      newForm(nil, options...),
		handlerFn: fn,
	}
}

type multipartForm struct {
	*form
	handlerFn func() MultiPartHandler
}

func (m *multipartForm) handlePart(h MultiPartHandler, fields url.Values, part *multipart.Part) (err error) {
	if part.FileName() == "" {
		return m.handleForm(fields, part)
	}

	tempfile, err := m.handleFile(h, part)
	if tempfile != "" {
		defer os.Remove(tempfile)
	}
	if err != nil {
		return err
	}
	if tempfile == "" {
		return nil
	}
	return h.HandleFile(part.FormName(), part.FileName(), tempfile)
}

func (m *multipartForm) handleFile(h MultiPartHandler, part *multipart.Part) (tempfilepath string, err error) {
	if m.conf.hasIgnore(part.FileName()) {
		return "", nil
	}

	if m.conf.isForbidden(part.FileName()) {
		return "", ErrForbiddenFileType
	}

	if !m.conf.hasCorrectFileType(part.FormName(), part.FileName()) {
		return "", NoAllowedFileTypeError(part.FormName())
	}

	/*
		TODO: change to a temp directory
	*/
	// ioutil.TempDir()
	dir := os.TempDir()

	tempfile, err := os.CreateTemp(dir, "example-upload-*.tmp")
	if err != nil {
		return "", ErrTempFile
	}

	defer tempfile.Close()
	err = m.readAll(part, tempfile)

	if err != nil {
		return tempfile.Name(), err
	}

	//log.Printf("Uploaded mimetype: %s", part.Header)
	return tempfile.Name(), nil
}

func (m *multipartForm) handleForm(fields url.Values, part *multipart.Part) (err error) {
	var bf bytes.Buffer
	err = m.readAll(part, &bf)

	if err != nil {
		return err
	}

	fields.Add(part.FormName(), bf.String())
	return nil
}

func (m *multipartForm) readAll(part *multipart.Part, wr io.Writer) (err error) {
	// continue reading until the whole file is upload or an error is reached

	var (
		filesize int
		uploaded bool
		n        int
		chunk    []byte
	)

	for {
		chunk = make([]byte, 4096)
		n, err = part.Read(chunk)

		switch {
		case err == io.EOF:
			uploaded = true
		case err != nil:
			return err
		}

		n, err = wr.Write(chunk[:n])

		switch {
		case err != nil:
			return err
		case filesize+n > m.conf.maxuploadSize:
			return ExceedsMaxFileSizeError(part.FormName())
		default:
			filesize += n
		}

		if uploaded {
			break
		}
	}

	return nil
}

func (m *multipartForm) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	h := m.handlerFn()

	if strings.ToUpper(strings.TrimSpace(r.Method)) != "POST" {
		h.HandleError(ErrInvalidRequestMethod, w)
		return
	}

	fields := url.Values{}

	var err error
	var mr *multipart.Reader
	var part *multipart.Part

	if mr, err = r.MultipartReader(); err != nil {
		h.HandleError(ErrNoMultipart, w)
		return
	}

	for {
		part, err = mr.NextPart()
		if err != nil {
			break
		}

		err = m.handlePart(h, fields, part)
		if err != nil {
			break
		}
	}

	if err == io.EOF {
		h.FinishForm(fields, w)
		return
	}

	if err != nil {
		h.HandleError(err, w)
	}
}
