# formhandler

formhandler offers an easy way to handle the upload of web forms, both simple and multipart forms.

## Example

see the examples directory for a complete example for a multipart form.

Documentation
-------------

see https://pkg.go.dev/gitlab.com/golang-utils/formhandler