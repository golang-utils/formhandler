package main

import (
	"fmt"
	"net/http"
	"os"
)

func main() {
	err := run()

	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %s", err.Error())
		os.Exit(1)
	}
	os.Exit(0)
}

func run() error {
	http.Handle("/upload", NewUploadHandler())
	http.Handle("/", http.HandlerFunc(handleIndex))

	host := "localhost:8080"
	fmt.Fprintln(os.Stdout, "listening on "+host)
	http.ListenAndServe(host, nil)
	return nil
}
