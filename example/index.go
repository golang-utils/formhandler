package main

import "net/http"

func handleIndex(wr http.ResponseWriter, req *http.Request) {
	wr.Write([]byte(indexHTML))
}

const indexHTML = `<!doctype html>
<html>
  <head lang="en">
    <meta charset="utf-8">
    <title>Formhandler Example</title>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  </head>
  <body>
    <div class="container">
      <div class="row">
          <div class="col-md-8">
            <h1>
              <a href="#" target="_blank">Formhandler Example</a>
            </h1>
            <hr> 
            <form id="form" action="/upload" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <label for="name">NAME</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Enter name" required />
              </div>
              <div class="form-group">
                <label for="email">EMAIL</label>
                <input type="email" class="form-control" id="email" name="email" placeholder="Enter email" />
              </div>
              <input id="uploadImage" type="file" accept="image/*" name="image" />
              <div id="confirm">
              </div>
							<input class="btn btn-success" type="submit" value="Upload">
            </form>
            <div id="err"></div>
            <hr>
          </div>
      </div>
    </div>
    <script type="text/javascript">
      $(document).ready(function (e) {
        $("#form").on('submit',(function(e) {
          e.preventDefault();
          $.ajax({
                url: "/upload",
                type: "POST",
                data:  new FormData(this),
                contentType: false,
                cache: false,
                processData:false,
                beforeSend : function() {
                  //$("#preview").fadeOut();
                  $("#err").fadeOut();
                },
                success: function(data) {
                  if(data=='invalid') {
                    // invalid file format.
                    $("#err").html("Invalid File !").fadeIn();
                  } else {
                    $("#confirm").html("successful upload").fadeIn();
                    $("#form")[0].reset(); 
                  }
                },
                error: function(e) {
                  console.log(e);
                  $("#err").html(e.responseText).fadeIn();
                }          
          });
        }));
      });
    </script>
  </body>
</html>
`
