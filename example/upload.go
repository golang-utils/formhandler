package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"os"

	"gitlab.com/golang-utils/formhandler"
)

func NewUploadHandler() http.Handler {
	return formhandler.NewMultiPart(
		func() formhandler.MultiPartHandler { return &handleUpload{} },
		formhandler.Accepted(formhandler.AcceptFor("image", formhandler.Images...)),
		formhandler.MaxUploadSize(1024*1024*10),
	)
}

type handleUpload struct{}

// HandleFile is called whenever a file is uploaded
// Any error returned here will be passed to HandleError
func (m *handleUpload) HandleFile(fieldname string, filename string, tempfile string) error {
	fmt.Fprintf(os.Stdout, "uploaded file: %q for field: %q tempfile: %q\n", filename, fieldname, tempfile)
	return nil
}

// FinishForm is called after all files have been uploaded.
// vals are the values that were entered to the form
// Any error returned here will be passed to HandleError
func (m *handleUpload) FinishForm(vals url.Values, w http.ResponseWriter) {
	err := m.validateForm(vals)
	if err != nil {
		m.HandleError(err, w)
		return
	}
	fmt.Fprintf(os.Stdout, "got form values: %v\n", vals)
	w.WriteHeader(200)
}

// HandleError is called whenever an error happens. HandleError takes care of reporting
// the errors back to the client
func (m *handleUpload) HandleError(err error, w http.ResponseWriter) {
	switch v := err.(type) {

	// print the form validation errors to the clients
	case ValidationError:
		w.WriteHeader(http.StatusBadRequest)
		enc := json.NewEncoder(w)
		enc.SetIndent("", " ")
		enc.Encode(v)

	// hide all other errors behind the starndard error message
	default:
		w.WriteHeader(500)
		fmt.Fprintln(w, "upload error")
	}
}

func (m *handleUpload) validateForm(vals url.Values) error {
	var ve ValidationError
	if vals.Get("email") == "" {
		ve.Message = "missing required fields"
		ve.Data = "email"
		return ve
	}
	return nil
}

type ValidationError struct {
	Message string
	Data    any
}

func (e ValidationError) Error() string {
	return fmt.Sprintf("%s: data: %#v", e.Message, e.Data)
}
