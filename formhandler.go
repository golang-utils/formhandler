package formhandler

import (
	"net/http"
	"net/url"
	"strings"
)

// Handler is the interface that the caller must define for a simple form
type Handler interface {
	// FinishForm is called after all files have been handled (if there were any)
	// - formvals are the values submitted via the form.
	// It is the job of the FinishForm method to do any data validation and write to the ResponseWriter the final message and header
	FinishForm(formvals url.Values, wr http.ResponseWriter)

	// HandleError is called whenever an error occurs. So it is the responsability for HandleError
	// to write the appropriate status code and error message to the client
	HandleError(error, http.ResponseWriter)
}

// New returns a http.Handler that can handle simple forms (without upload files).
// If you need to upload files, define a multipart form and call NewMultiPart.
func New(fn func() Handler, options ...Option) http.Handler {
	return newForm(fn, options...)
}

func newForm(fn func() Handler, options ...Option) *form {
	conf := defaultConf()

	for _, opt := range options {
		opt(conf)
	}

	return &form{
		handlerFn: fn,
		conf:      conf,
	}
}

type form struct {
	handlerFn func() Handler
	conf      *conf
}

func (m *form) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	h := m.handlerFn()

	if strings.ToUpper(strings.TrimSpace(r.Method)) != "POST" {
		//	log.Printf("%s: %q", ErrInvalidRequestMethod.Error(), r.Method)
		h.HandleError(ErrInvalidRequestMethod, w)
		return
	}

	h.FinishForm(r.PostForm, w)
}
