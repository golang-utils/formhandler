package formhandler

import (
	"path/filepath"
	"strings"
)

// Option is an option for either a simple form with New or a multipart form with NewMultiPart
type Option func(*conf)

var (
	Images    = []string{"jpg", "jpeg", "bmp", "png", "tif", "tiff", "gif"}
	Documents = []string{"doc", "docx", "pdf", "xls", "xlsx", "txt", "ppt", "pptx", "rtf", "pub", "one", "odt", "ods", "odp", "odg"}

	// Executables are forbidden by default, to allow them, pass the Forbidden option with the extensions
	// that are forbidden (or Forbidden without parameters, if all files should be allowed)
	// for a more complete list, see https://www.lifewire.com/list-of-executable-file-extensions-2626061
	Executables = []string{"exe", "com", "bin", "bat", "cmd", "inf", "ipa", "osx", "pif", "run", "wsh", "sh", "apk", "app", "cab", "csh", "cpl", "ex", "inf", "msi", "out", "reg", "vb", "vbs", "ws", "wsf", "0XE", "jar"}
)

// Exts joins collection of extensions
func Exts(collections ...[]string) []string {
	var result []string

	for _, col := range collections {
		result = append(result, col...)
	}

	return result
}

// AcceptFor defines accepted extensions for a form field
// - fieldname is the name of the form field. it is matched exactly (case sensitive)
// - ext is/are the accepted file extension(s) (without the dot), if ext is * then every type of file is allowed
// file extensions are always matched lowercase, so no need to specify uppercase variants as well.
// To facilitate common extensions there are helpers like Images and Documents
func AcceptFor(fieldname string, ext ...string) acceptedFor {
	return acceptedFor{
		field:      fieldname,
		extensions: ext,
	}
}

// Accepted sets the accepted filetypes via AcceptedFor(s).
// Use the AcceptFor function to create AcceptedFor(s)AcceptedFor
// The forbidden filetypes (Executables by default) are always checked before the Accepted ones.
// The forbidden ones can be set via the Forbidden option.
func Accepted(acceptedFiletypes ...acceptedFor) Option {
	return func(opts *conf) {
		opts.acceptedFileTypes = acceptedFiletypes
	}
}

// Forbidden sets the forbidden filetypes. By default, the Executables are forbidden, but that can
// be overritten with this Option.
// Forbidden is always checked before the allowed ones.
func Forbidden(forbiddenFileTypes ...string) Option {
	return func(opts *conf) {
		opts.forbiddenFileTypes = forbiddenFileTypes
	}
}

// MAX_UPLOAD_SIZE is the default maximum upload file size (can be overwritten with the MaxUploadSize Option )
const MAX_UPLOAD_SIZE = 1024 * 1024 * 7

// MaxUploadSize sets the maximum allowed upload size (default is MAX_UPLOAD_SIZE)
func MaxUploadSize(size int) Option {
	return func(opts *conf) {
		opts.maxuploadSize = size
	}
}

func Ignore(filenames ...string) Option {
	return func(opts *conf) {
		opts.ignores = filenames
	}
}

type conf struct {
	maxuploadSize      int
	acceptedFileTypes  []acceptedFor // the accepted file types
	forbiddenFileTypes []string      // forbidden has precedence before accepted
	ignores            []string      // ignore filenames
}

func (c *conf) isForbidden(fileName string) bool {
	ext := strings.ToLower(filepath.Ext(fileName))

	for _, forbidden := range c.forbiddenFileTypes {
		if ext == strings.ToLower("."+forbidden) {
			return true
		}
	}

	return false
}

func (c *conf) hasIgnore(filename string) bool {
	for _, ign := range c.ignores {
		if ign == filename {
			return true
		}
	}

	return false
}

func (c *conf) hasCorrectFileType(fieldName string, fileName string) bool {

	var found *acceptedFor

	for _, acc := range c.acceptedFileTypes {
		if strings.HasPrefix(fieldName, acc.field) {
			found = &acc
		}
	}

	if found == nil {
		return false
	}

	ext := strings.ToLower(filepath.Ext(fileName))

	for _, accepted := range found.extensions {
		if ext == strings.ToLower("."+accepted) {
			return true
		}
	}

	return false
}

func defaultConf() *conf {
	return &conf{
		maxuploadSize:      MAX_UPLOAD_SIZE,
		forbiddenFileTypes: Executables,
	}
}

type acceptedFor struct {
	field      string
	extensions []string
}
